#include <stdio.h>
#include <unistd.h>

static int fd;
static void opendsp() {
	fd = 0;
#if 0
	fd = open ("/dev/dsp", 0);
	if (fd == -1) {
		fprintf (stderr, "Cannot open dsp\n");
		exit (1);
	}
#endif
}

static void closedsp() {
	close (fd);
}

/* return when there's some noise */
static int waitfornoise() {
	int level, noise = 0;
	int hasnoise = 0;
	char buf[128];
	int i, ret = read (fd, buf, sizeof (buf));
	if (ret<1) {
		fprintf (stderr, "WTF\n");
		exit (1);
	}
	for (i=0; i<ret; i++) {
		if (buf[i] != 0x7f)
			noise++;
	}
	level = (noise*100)/ret;
//printf ("%02x %02x %02x\n", buf[0], buf[1], buf[2]);
	if (level> 0) {
		printf ("%03d  ", level);
		for (i=0;i<level;i++) printf ("#");
		printf ("\n");
	//	printf ("level %d\n", level);
		hasnoise = 1;
	}

	return hasnoise;
}

main() {
	opendsp ();
	for (;;) {
		if (!waitfornoise ())
			continue;
//		printf ("Noise!\n");
	}
	closedsp ();
}
